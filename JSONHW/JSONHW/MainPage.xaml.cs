﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace JSONHW
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        async void GetWordInformation(object sender, System.EventArgs e)
        {
            //This will make sure the Entry is not blank to ensure it wont search up null values
            if(!string.IsNullOrWhiteSpace(findWord.Text))
            {
                string word = findWord.Text;
                HttpClient client = new HttpClient();

                var uri = new Uri(
                    string.Format(
                        $"https://owlbot.info/api/v2/dictionary/" + word));

                //Will ask for a request to use API follow the above URL
                var request = new HttpRequestMessage();
                request.Method = HttpMethod.Get;
                request.RequestUri = uri;

                //This will wait for a successful response from the service
                HttpResponseMessage response = await client.SendAsync(request);
                DictionaryData[] dictonaryData = null;
                //If succesful it will pull in all sent data from client and display it on screen
                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    dictonaryData = DictionaryData.FromJson(content);

                    //Type.Text = dictonaryData.Type;
                    //Def.Text = dictonaryData.DefinitionDefinition;
                    //ex.Text = dictonaryData.Example;
                }
;
            }

        }   

    }
}
