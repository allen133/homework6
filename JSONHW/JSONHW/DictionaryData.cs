﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JSONHW
{
    using System;
    using System.Collections.Generic;

    using System.Globalization;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    public partial class DictionaryData
    {
        [JsonProperty("definitions")]
        public Definition[] Definitions { get; set; }

        [JsonProperty("word")]
        public string Word { get; set; }

        [JsonProperty("pronunciation")]
        public string Pronunciation { get; set; }
    }

    public partial class Definition
    {
        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("definition")]
        public string DefinitionDefinition { get; set; }

        [JsonProperty("example")]
        public string Example { get; set; }

        [JsonProperty("image_url")]
        public object ImageUrl { get; set; }

        [JsonProperty("emoji")]
        public object Emoji { get; set; }
    }

    public partial class DictionaryData
    {
        public static DictionaryData[] FromJson(string json) => JsonConvert.DeserializeObject<DictionaryData[]>(json, JSONHW.Converter.Settings);
    }

    public static class Serialize
    {
        public static string ToJson(this DictionaryData self) => JsonConvert.SerializeObject(self, JSONHW.Converter.Settings);
    }

    internal static class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters =
            {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }
}
